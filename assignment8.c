#include<stdio.h>

struct student_details
{
    char chr[50];
    char sub[50];
    int mrk;
};

int main()
{
    struct student_details std[5];
    int i;
    printf("\n");
    for(i=0; i<5; i++)
    {
        printf("Enter name of student %d: ",i+1);
        scanf("%s",&std[i].chr);
        printf("Enter subject of student %d: ",i+1);
        scanf("%s",&std[i].sub);
        printf("Enter marks of student %d: ",i+1);
        scanf("%d",&std[i].mrk);
        printf("\n");
    }
    printf("\n");
    printf("Details of students are shown below\n");
    printf("\n");
    for(i=0; i<5; i++)
    {
        printf("Name of student %d: %s \n",i+1,std[i].chr);
        printf("Subject of student %d: %s \n",i+1,std[i].sub);
        printf("Marks of student %d: %d\n",i+1,std[i].mrk);
        printf("\n");
    }
    return 0;
}
